package es.deusto;

import org.junit.Test;
import static org.junit.Assert.*;
import org.easymock.EasyMock;

public class UtilidadesCalendarioTest {
	
	@Test
	public void testSumar() {
		UtilidadesCalendario uc = new UtilidadesCalendario();
		
		assertEquals(uc.sumar(0, 2), 2);
		assertEquals(uc.sumar(2, 0), 2);
		assertEquals(uc.sumar(1, 2), 3);
	}
	
	@Test
	public void testMinutosImpares() {
		// testear el m'etodo getMinutosImpares, utilizando EasyMock
		ITimeService ts = EasyMock.createMock(ITimeService.class);
		
		EasyMock.expect(ts.getTime()).andReturn(60000L).once();
		EasyMock.expect(ts.getTime()).andReturn(120000L).once();
		
		EasyMock.replay(ts);
		
		UtilidadesCalendario uc = new UtilidadesCalendario(ts);
		assertTrue(uc.getMinutoImpar());
		assertFalse(uc.getMinutoImpar());
		
		EasyMock.verify(ts);
	}
	
	@Test
	public void testGetMaximoDia() throws Exception {
		// testear el m'etodo getMaximoDia, pudiendo cambiar algo de c'odigo si hay problemas
		assertEquals(UtilidadesCalendario.getMaximoDia("2008-02"), 29);
		assertEquals(UtilidadesCalendario.getMaximoDia("2011-02"), 28);
		assertEquals(UtilidadesCalendario.getMaximoDia("2011-11"), 30);
		assertEquals(UtilidadesCalendario.getMaximoDia("2010-12"), 31);
	}
}
