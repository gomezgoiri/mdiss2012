package es.deusto;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class UtilidadesCalendario {

	private ITimeService timeService;
	
	public UtilidadesCalendario() {
		this.timeService = new TimeService();
	}
	
	public UtilidadesCalendario(ITimeService tm) {
		this.timeService = tm;
	}
	
	public int sumar(int sumando1, int sumando2) {
		return sumando1 + sumando2;
	}
	
	public boolean getMinutoImpar() {
		return this.timeService.getTime()/60000 % 2 != 0;
	}
	
	public static int getMaximoDia(String dateString) throws ParseException {
		// Parseamos la fecha [esto est'a bien]
		final SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM");
		final Date date = formatter.parse(dateString);
		final Calendar cal = Calendar.getInstance();
		cal.setTime(date);
		
		final int year  = cal.get(Calendar.YEAR);
		final int month = cal.get(Calendar.MONTH) + 1; //The months are from 0 to 11
		
		// Enero, Marzo, Mayo, Julio, Agosto, Octubre, Diciembre
		if(month == 1 || month == 3 || month == 5 || month == 7 || month == 8 || month == 10 || month == 12)
			return 31;
		
		// Abril, Junio, Septiembre, Noviembre
		if(month == 4 || month == 6 || month == 9 || month == 11)
			return 30;
		
		// Si es bisiesto
		if(year % 500 == 0 || (year % 4 == 0 && year % 100 != 0))
			return 29;
		
		return 28;
	}
	
}
