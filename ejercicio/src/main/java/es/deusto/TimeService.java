package es.deusto;

public class TimeService implements ITimeService
{

	@Override
	public long getTime()
	{
		return System.currentTimeMillis();
	}

}
